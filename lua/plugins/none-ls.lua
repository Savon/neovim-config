return {
  "nvimtools/none-ls.nvim",
  config = function()
    -- none-ls is a fork of null-ls
    local null_ls = require("null-ls")

    null_ls.setup({
      -- install throught Mason
      sources = {
        -- lua
        null_ls.builtins.formatting.stylua,
        --TS/JS
        null_ls.builtins.diagnostics.eslint_d,
        null_ls.builtins.formatting.prettier,
      },
    })

    vim.keymap.set("n", "<leader>lf", vim.lsp.buf.format)
  end,
}
